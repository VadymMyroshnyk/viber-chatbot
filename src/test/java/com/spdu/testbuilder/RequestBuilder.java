package com.spdu.testbuilder;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;

public class RequestBuilder {
    private final RequestDto request = new RequestDto();

    public static RequestBuilder aRequest() {
        return new RequestBuilder();
    }

    public RequestBuilder event(String event) {
        request.setEvent(event);
        return this;
    }

    public RequestBuilder sender(String id) {
        Sender sender = Sender.builder()
            .id(id)
            .build();
        request.setSender(sender);
        return this;
    }

    public RequestBuilder message(String text) {
        Message message = Message.builder()
            .text(text)
            .build();
        request.setMessage(message);
        return this;
    }

    public RequestDto build() {
        return request;
    }
}
