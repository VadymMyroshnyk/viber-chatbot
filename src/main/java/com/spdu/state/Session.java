package com.spdu.state;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.entity.HelpRequest;
import com.spdu.service.Context;

public class Session {
    private final Sender sender;
    private BotState state;
    private HelpRequest helpRequest;
    private Context context;

    public Session(Sender sender) {
        this.sender = sender;
    }

    public String handle(RequestDto request) {
        final String nameOfAction = request.getMessage().getText();
        final Action action = Action.of(nameOfAction);
        state = state.handle(action, request);
        return state.getMessage(request.getSender());
    }

    public void setState(BotState state) {
        this.state = state;
    }

    public HelpRequest getHelpRequest() {
        return helpRequest;
    }

    public void setHelpRequest(HelpRequest helpRequest) {
        this.helpRequest = helpRequest;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Sender getSender() {
        return sender;
    }
}
