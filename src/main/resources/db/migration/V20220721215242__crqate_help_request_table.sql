CREATE TABLE help_request
(
    id             SERIAL       NOT NULL,
    help_recipient VARCHAR(255) NOT NULL,
    title          VARCHAR(255) NOT NULL,
    amount         VARCHAR(255) NOT NULL,
    card_number    VARCHAR(20)  NOT NULL,
    contact        VARCHAR(255) NOT NULL,
    phone          VARCHAR(20)  NOT NULL,
    photo          VARCHAR(255) NOT NULL,
    create_date    TIMESTAMP    NOT NULL
);