package com.spdu.service.communication.message;

import com.spdu.dto.Sender;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.state.Action;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.spdu.state.TagBuilder.blueFont;

@Service
public class UiMessageFactory implements MessageFactory {
    @Override
    public Message mainMenu(Sender sender) {
        List<Button> buttons = List.of(
            createButton("Потрібна допомога", Action.NEED_HELP.name()),
            createButton("Можу допомогти", Action.CAN_HELP.name()),
            createButton("Статистика", Action.STATISTIC.name()),
            createButton("Інфо", Action.INFO.name())
        );

        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(buttons)
            .build();

        return Message.builder()
            .receiver(sender.getId())
            .text("""
                      🔻Як працює цей бот?
                                  
                      🔘"Потрібна допомога"
                      Якщо вам потрібна допомога у зборі коштів.
                      Бот поділиться вашим запитом із користувачами та допоможе
                      зібрати необхідну суму.
                                  
                      🔘"Можу допомогти"
                      Якщо ви хочете допомогти у зборі коштів.
                      Бот поділиться з вами запитами та реквізитами від користувачів, аби ви могли швидко
                      та зручно підтримати збір коштів.""")
            .keyboard(keyboard)
            .build();
    }

    @Override
    public Message info(Sender sender) {
        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton(Action.MAIN_MENU.name())))
            .build();

        return Message.builder()
            .receiver(sender.getId())
            .text("""
                      ☎У цьому розділі ми зібрали контакти благойдійних проєктів та фондів, які допомагають армії,
                      аби вони завжди були під рукою.
                                  
                      🖇З ними можна звʼязатися для запиту на збір коштів, підтримати від вашої компанії, записатися
                      у волонтери, або якщо у боті закінчаться запити, то в вас буде можливість знайти куди закинути
                      донат на добру справу:
                                  
                      📌Фонд "Повернись Живим"
                      сайт: https://savelife.in.ua/
                      телефон: +38 (068) 500-88-00
                                  
                      📌Фонд "Сергія Притули"
                      сайт: https://prytulafoundation.org/
                      телефон: +38 073 310 40 70
                                  
                      📌Фонд «Архангел Світла»
                      сайт: https://archangel-of-light.org.ua/
                      телефон: 080-033-51-43
                                  
                      📌Фонд "Дім"
                      сайт: https://dimcharity.com/
                      телефон: +38 (0800) 357205
                                  
                      📌Фонд допомоги армії «Крила Фенікса»
                      сайт: http://wings-phoenix.org.ua/uk/
                      телефон: (044) 364 50 60
                                  
                      📌Фонд оперативної допомоги "KOLO"
                      сайт: https://www.koloua.com/
                      імейл: info@koloua.com
                                  
                      Допоможемо разом! 🇺🇦
                      """)
            .keyboard(keyboard)
            .build();
    }

    @Override
    public Message needHelp(Sender sender) {
        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton(Action.MAIN_MENU.name())))
            .build();

        return Message.builder()
            .receiver(sender.getId())
            .text("""
                      ✅Кому потрібна допомога?
                      Напишіть дані тих, кому збираєте кошти.
                                      
                      💡Приклад:
                      ТРО 188 бригади м.Черкас
                      """)
            .keyboard(keyboard)
            .build();
    }

    private Button createButton(String name, String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont(name))
            .actionBody(actionBody)
            .columns(3)
            .build();
    }

    private Button createButton(String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont("Меню"))
            .actionBody(actionBody)
            .columns(6)
            .build();
    }
}
