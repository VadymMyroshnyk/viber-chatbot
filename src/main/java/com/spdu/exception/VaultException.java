package com.spdu.exception;

public class VaultException extends RuntimeException {

    public VaultException(String message) {
        super(message);
    }
}
