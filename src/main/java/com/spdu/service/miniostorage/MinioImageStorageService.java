package com.spdu.service.miniostorage;

import com.spdu.service.ImageStorageService;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MinioImageStorageService implements ImageStorageService {

    @Value("${minio.bucket.name}")
    private String bucketName;

    private final MinioClient minioClient;

    @Override
    @SneakyThrows
    public String upload(String imgUrl) {
        URL url = new URL(imgUrl);
        Path tempFile = Files.createTempFile(UUID.randomUUID().toString(), ".jpg");
        try (InputStream in = url.openStream()) {
            Files.copy(in, tempFile, StandardCopyOption.REPLACE_EXISTING);
        }

        String filename = tempFile.getFileName().toString();

        UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
            .bucket(bucketName)
            .object(filename)
            .filename(tempFile.toString())
            .build();

        minioClient.uploadObject(uploadObjectArgs);

        Files.deleteIfExists(tempFile);

        return filename;
    }
}
