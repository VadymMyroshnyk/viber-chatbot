package com.spdu.testdouble;

import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;
import com.spdu.service.communication.message.MessageFactory;

public class MessageFactoryStub implements MessageFactory {
    @Override
    public Message mainMenu(Sender sender) {
        return getMessage("main-menu", sender);
    }

    @Override
    public Message info(Sender sender) {
        return getMessage("info", sender);
    }

    @Override
    public Message needHelp(Sender sender) {
        return getMessage("need-help", sender);
    }

    private Message getMessage(String text, Sender sender) {
        return Message.builder()
            .text(text)
            .receiver(sender.getId())
            .build();
    }
}
