package com.spdu.testconfig;

import com.spdu.service.communication.message.MessageFactory;
import com.spdu.testdouble.MessageDeliveryServiceSpy;
import com.spdu.testdouble.MessageFactoryStub;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class TestConfig {
    @Bean
    @Primary
    MessageDeliveryServiceSpy messageDeliveryServiceSpy() {
        return new MessageDeliveryServiceSpy();
    }

    @Bean
    @Primary
    MessageFactory messageFactory() {
        return new MessageFactoryStub();
    }
}
