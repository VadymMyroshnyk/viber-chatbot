package com.spdu.service;

import java.util.Arrays;

enum CallbackType {
    DELIVERED,
    SEEN,
    FAILED,
    SUBSCRIBED,
    UNSUBSCRIBED,
    CONVERSATION_STARTED,
    MESSAGE,
    NONE;

    public static CallbackType of(String eventName) {
        return Arrays.stream(values())
            .filter(value -> value.name().equalsIgnoreCase(eventName))
            .findFirst().orElse(NONE);
    }
}
