package com.spdu.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate(
        RestTemplateBuilder restTemplateBuilder,
        @Value("${rest.template.connection.timeout}") Duration readTimeout,
        @Value("${rest.template.request.timeout}") Duration connectTimeout
    ) {
        return restTemplateBuilder
            .setConnectTimeout(connectTimeout)
            .setReadTimeout(readTimeout)
            .build();
    }
}
