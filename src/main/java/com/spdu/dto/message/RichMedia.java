package com.spdu.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RichMedia {

    @JsonProperty("Type")
    @Builder.Default
    private String type = "rich_media";
    @JsonProperty("ButtonsGroupColumns")
    @Builder.Default
    private int buttonsGroupColumns = 6;
    @JsonProperty("ButtonsGroupRows")
    @Builder.Default
    private int buttonsGroupRows = 7;
    @JsonProperty("BgColor")
    @Builder.Default
    private String bgColor = "#FFFFFF";
    @JsonProperty("Buttons")
    private List<Button> buttons;
}
