package com.spdu.state.info;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

public class InfoState extends BotState {

    public InfoState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        if (Action.MAIN_MENU.equals(action)) {
            return new MainMenuState(session);
        }

        return this;
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        final Message info = session.getContext()
            .messageFactory()
            .info(sender);
        return objectMapper.writeValueAsString(info);
    }
}
