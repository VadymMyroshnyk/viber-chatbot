package com.spdu.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.config.Vault;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class ViberBotListener implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViberBotListener.class);

    @Value("${viber.bot.webhook.url}")
    private String webhookUrl;
    @Value("${server.url}")
    private String serverUrl;
    @Value("${viber.bot.header}")
    private String tokenHeaderName;

    private final ObjectMapper objectMapper;
    private final VaultService vaultService;
    private final RestTemplate restTemplate;

    public ViberBotListener(VaultService vaultService, RestTemplate restTemplate) {
        this.vaultService = vaultService;
        this.restTemplate = restTemplate;
        this.objectMapper = new ObjectMapper();
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(@NonNull ApplicationReadyEvent event) {
        String body = objectMapper.writeValueAsString(createWebHookParams());
        HttpEntity<String> entity = new HttpEntity<>(body, getHeaders());

        ResponseEntity<String> response = restTemplate.exchange(webhookUrl, HttpMethod.POST, entity, String.class);

        LOGGER.info("Connection to Viber status {}, body {}", response.getStatusCode(), response.getBody());
    }

    private HttpHeaders getHeaders() {
        Vault vaultProperties = vaultService.getVaultProperties();
        HttpHeaders headers = new HttpHeaders();
        headers.set(tokenHeaderName, vaultProperties.getToken());

        return headers;
    }

    private Map<String, Object> createWebHookParams() {
        return Map.of(
            "url", serverUrl,
            "send_name", true,
            "send_photo", true,
            "event_types", getEvents()
        );
    }

    private List<String> getEvents() {
        return List.of("delivered", "seen", "failed", "subscribed", "unsubscribed", "conversation_started", "message");
    }
}
