package com.spdu.testconfig;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.vault.VaultContainer;

public class TestContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public static final String TEST_SECRET_TOKEN = "test-secret-token";
    private static final String TEST_ROOT_VAULT_TOKEN = "test-root-token";
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:14");
    private static final VaultContainer<?> vaultContainer = new VaultContainer<>("vault")
        .withVaultToken(TEST_ROOT_VAULT_TOKEN)
        .withSecretInVault("secret/viber", "token=" + TEST_SECRET_TOKEN);

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        postgres.start();
        vaultContainer.start();
        ViberServerMock.stubPost("/pa/set_webhook");

        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
            applicationContext,
            "spring.datasource.url=" + postgres.getJdbcUrl(),
            "spring.datasource.username=" + postgres.getUsername(),
            "spring.datasource.password=" + postgres.getPassword(),
            "vault.port=" + vaultContainer.getFirstMappedPort(),
            "vault.token=" + TEST_ROOT_VAULT_TOKEN,
            "viber.bot.url=http://localhost:" + ViberServerMock.port() + "/pa"
        );
    }
}
