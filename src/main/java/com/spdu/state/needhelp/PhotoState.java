package com.spdu.state.needhelp;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.entity.HelpRequest;
import com.spdu.service.Context;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

import java.time.LocalDateTime;
import java.util.List;

import static com.spdu.state.TagBuilder.blueFont;

public class PhotoState extends BotState {

    public PhotoState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        final MainMenuState newState = new MainMenuState(session);
        return switch (action) {
            case MAIN_MENU -> newState;
            case USER_INPUT -> {
                final String media = request.getMessage()
                    .getMedia();

                if (media == null) {
                    yield newState;
                }

                Context context = session.getContext();
                String filename = context.imageStorageService().upload(media);
                saveHelpRequest(filename);
                yield newState;
            }
            default -> this;
        };
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton(Action.MAIN_MENU.name())))
            .build();

        Message message = Message.builder()
            .receiver(sender.getId())
            .text("\uD83D\uDCF7Фото предмету збору")
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(message);
    }

    private Button createButton(String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont("Меню"))
            .actionBody(actionBody)
            .columns(6)
            .build();
    }

    private void saveHelpRequest(String filename) {
        HelpRequest helpRequest = session.getHelpRequest();
        helpRequest.setPhoto(filename);
        helpRequest.setCreateDate(LocalDateTime.now());
        Context context = session.getContext();
        context.helpRequestRepository().save(helpRequest);
        session.setHelpRequest(null);
    }
}