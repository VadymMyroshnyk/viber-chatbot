package com.spdu.state.statistics;

import com.spdu.dto.message.Button;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

import java.util.List;

public class StatisticState extends BotState {

    public StatisticState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        if (Action.MAIN_MENU.equals(action)) {
            return new MainMenuState(session);
        }

        return this;
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton(Action.MAIN_MENU.name())))
            .build();

        Message message = Message.builder()
            .receiver(sender.getId())
            .text(getText())
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(message);
    }

    private Button createButton(String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text("<font color=\"#1133dd\">Меню</font>")
            .actionBody(actionBody)
            .columns(6)
            .build();
    }

    private String getText() {
        return """
            🔻За час існування бота
            "Допомагаємо Разом",
            до нас звернулося 46574 людей.
            """;
    }
}
