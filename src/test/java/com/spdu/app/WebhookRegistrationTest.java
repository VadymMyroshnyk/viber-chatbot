package com.spdu.app;

import com.spdu.testconfig.AppTest;
import com.spdu.testconfig.TestContextInitializer;
import com.spdu.testconfig.ViberServerMock;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

@AppTest
class WebhookRegistrationTest {
    @Test
    @DisplayName("On application ready event Viber webhook should be registered")
    void registerWebhook() {
        ViberServerMock.verifyCalled(
            postRequestedFor(urlEqualTo("/pa/set_webhook"))
                .withHeader("X-Viber-Auth-Token", equalTo(TestContextInitializer.TEST_SECRET_TOKEN))
        );
    }
}
