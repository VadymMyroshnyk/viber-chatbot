package com.spdu.testconfig;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;

import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class ViberServerMock {
    private static final WireMockServer server = new WireMockServer(wireMockConfig().dynamicPort());

    static {
        server.start();
    }

    private ViberServerMock() {
        // hide public constructor
    }

    public static int port() {
        return server.port();
    }

    public static void verifyCalled(RequestPatternBuilder builder) {
        server.verify(builder);
    }

    public static void stubPost(String url) {
        server.stubFor(post(url).willReturn(ok()));
    }
}
