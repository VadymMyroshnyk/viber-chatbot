# viber-chatbot

## Tech, Framework and other Dependencies

* Java version: **17**
* Gradle version: **7.4.1**
* SpringBoot version: **2.7.2**
* Database: **PostgreSQL**
* Other: **Spring Data JPA**, **Lombok**, **JUnit 5**, **Docker**

## Ngrok

<p>
<details>
<summary>Instructions for running Ngrok</summary>

ngrok.yml
```shell
version: "2"
authtoken: ngrok token
tunnels:
  first:
    addr: 9000
    proto: http    
  second:
    addr: 8080
    proto: http
```

Use docker-compose
```shell
./ngrok start --all
```

</details>
</p>

## Docker

<p>
<details>
<summary>Instructions for running Vault, PostgreSQL, Minio</summary>

open directory
```shell
cd .local
```

Use docker-compose
```shell
docker-compose up
```

Vault token
```shell
dev-only-token
```

</details>
</p>

## Building

<p>
<details>
<summary>Instructions for building, running and testing project</summary>


build the project
```shell
./gradlew :build
```

run the project
```shell
./gradlew :bootRun
```

run tests
```shell
./gradlew :test
```

</details>
</p>