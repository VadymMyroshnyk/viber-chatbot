package com.spdu.testdouble;

import com.spdu.service.communication.MessageDeliveryService;

public class MessageDeliveryServiceSpy implements MessageDeliveryService {

    private String calledWith = "";

    @Override
    public void send(String message) {
        if (!calledWith.isEmpty()) {
            calledWith += '\n';
        }
        calledWith += message;
    }

    public String getCalledWith() {
        final String result = calledWith;
        calledWith = "";
        return result;
    }
}
