package com.spdu.service;

import com.spdu.repository.HelpRequestRepository;
import com.spdu.service.communication.message.MessageFactory;

public record Context(ImageStorageService imageStorageService,
                      HelpRequestRepository helpRequestRepository,
                      String minioBucketPath,
                      MessageFactory messageFactory) {
}
