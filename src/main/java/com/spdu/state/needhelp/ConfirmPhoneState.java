package com.spdu.state.needhelp;

import com.spdu.dto.RequestDto;
import com.spdu.entity.HelpRequest;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Contact;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.dto.Sender;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

import java.util.List;

import static com.spdu.state.TagBuilder.blueFont;

public class ConfirmPhoneState extends BotState {

    protected ConfirmPhoneState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        return switch (action) {
            case MAIN_MENU -> new MainMenuState(session);
            case PHONE -> {
                savePhone(request.getMessage());
                yield new PhotoState(session);
            }
            default -> this;
        };
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        List<Button> buttons = List.of(
            createButton("Меню", Action.MAIN_MENU.name(), "reply"),
            createButton("Підтвердити номер", Action.PHONE.name(), "share-phone")
        );

        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(buttons)
            .build();

        Message message = Message.builder()
            .receiver(sender.getId())
            .text("☎Підтвердіть Ваш номер телефону")
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(message);
    }

    private Button createButton(String name, String actionBody, String actionType) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont(name))
            .actionBody(actionBody)
            .actionType(actionType)
            .columns(3)
            .build();
    }

    private void savePhone(Message message) {
        Contact contact = message.getContact();
        HelpRequest helpRequest = session.getHelpRequest();
        helpRequest.setPhone(contact.getPhoneNumber());
    }
}