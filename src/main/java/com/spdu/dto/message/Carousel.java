package com.spdu.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Carousel {

    private String receiver;
    @Builder.Default
    private String type = "rich_media";
    @JsonProperty("min_api_version")
    @Builder.Default
    private int minApiVersion = 7;
    @JsonProperty("rich_media")
    private RichMedia richMedia;
    private MessageKeyboard keyboard;
}
