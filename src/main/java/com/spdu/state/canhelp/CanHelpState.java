package com.spdu.state.canhelp;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Carousel;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.dto.message.RichMedia;
import com.spdu.entity.HelpRequest;
import com.spdu.service.Context;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

import static com.spdu.state.TagBuilder.blueFont;

public class CanHelpState extends BotState {

    public CanHelpState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        if (Action.MAIN_MENU.equals(action)) {
            return new MainMenuState(session);
        }

        return this;
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        List<HelpRequest> helpRequests = getHelpRequests(session.getContext());

        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton("Меню", Action.MAIN_MENU.name())))
            .build();

        if (helpRequests.isEmpty()) {
            Message message = Message.builder()
                .receiver(sender.getId())
                .text("\uD83D\uDD3BЗапити на допомогу відсутні")
                .keyboard(keyboard)
                .build();

            return objectMapper.writeValueAsString(message);
        }

        RichMedia richMedia = RichMedia.builder()
            .buttons(getButtons(helpRequests))
            .build();

        Carousel carousel = Carousel.builder()
            .receiver(sender.getId())
            .richMedia(richMedia)
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(carousel);
    }

    private List<HelpRequest> getHelpRequests(Context context) {
        Pageable sortedByCreateDateDesc = PageRequest.of(0, 3, Sort.by("createDate").descending());
        Page<HelpRequest> helpRequests = context.helpRequestRepository().findAll(sortedByCreateDateDesc);

        return helpRequests.stream()
            .toList();
    }

    private List<Button> getButtons(List<HelpRequest> helpRequests) {
        return helpRequests.stream()
            .map(this::getButtons)
            .flatMap(Collection::stream)
            .toList();
    }

    private List<Button> getButtons(HelpRequest helpRequest) {
        return List.of(
            createTextButton(helpRequest.getHelpRecipient()),
            createTextButton(helpRequest.getTitle()),
            createTextButton(helpRequest.getAmount()),
            createTextButton(helpRequest.getCardNumber()),
            createTextButton(helpRequest.getContact()),
            createTextButton(helpRequest.getPhone()),
            createButton("Повна інформіція", Action.CAN_HELP.name()),
            createButton(String.format("%s%s", session.getContext().minioBucketPath(), helpRequest.getPhoto()))
        );
    }

    private Button createButton(String name, String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont(name))
            .actionBody(actionBody)
            .columns(6)
            .build();
    }

    private Button createTextButton(String text) {
        return Button.builder()
            .text(blueFont(text))
            .actionBody("")
            .columns(6)
            .bgColor("#ffffff")
            .build();
    }

    private Button createButton(String img) {
        return Button.builder()
            .bgMediaType("picture")
            .bgMediaScaleType("crop")
            .bgMedia(img)
            .heightScale(100)
            .columns(6)
            .rows(7)
            .actionBody("")
            .build();
    }
}
