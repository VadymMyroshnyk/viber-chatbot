package com.spdu.service.communication.message;

import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;

public interface MessageFactory {

    Message mainMenu(Sender sender);

    Message info(Sender sender);

    Message needHelp(Sender sender);
}
