package com.spdu.broadcast;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Builder
@Data
public class Broadcast {
    @JsonProperty("broadcast_list")
    private Collection<String> users;
    @JsonProperty("min_api_version")
    @Builder.Default
    private int minApiVersion = 6;
    @Builder.Default
    private String type = "text";
    private String text;
}
