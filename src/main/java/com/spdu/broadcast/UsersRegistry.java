package com.spdu.broadcast;

import com.spdu.dto.Sender;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UsersRegistry {
    private final Set<String> userIds = ConcurrentHashMap.newKeySet();

    public void register(Sender sender) {
        userIds.add(sender.getId());
    }

    public Collection<String> getAllIds() {
        return new HashSet<>(userIds);
    }
}
