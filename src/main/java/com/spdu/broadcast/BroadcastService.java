package com.spdu.broadcast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.config.Vault;
import com.spdu.service.VaultService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

@Service
public class BroadcastService {

    @Value("${viber.bot.header}")
    private String tokenHeaderName;
    @Value("${viber.bot.broadcast.url}")
    private String broadcastUrl;

    private final UsersRegistry usersRegistry;
    private final RestTemplate restTemplate;
    private final VaultService vaultService;
    private final ObjectMapper objectMapper;

    public BroadcastService(UsersRegistry usersRegistry,
                            RestTemplate restTemplate,
                            VaultService vaultService) {
        this.usersRegistry = usersRegistry;
        this.restTemplate = restTemplate;
        this.vaultService = vaultService;
        this.objectMapper = new ObjectMapper();
    }

    @SneakyThrows
    public void hello() {
        Collection<String> users = usersRegistry.getAllIds();
        Broadcast broadcast = Broadcast.builder()
            .users(users)
            .text("""
                Привіт, я чат-бот Допомагаємо Разом! "
                "Готовий приймати запити
                та разом збирати кошти на добрі справи! 🇺🇦
                """)
            .build();

        String message = objectMapper.writeValueAsString(broadcast);

        sendMessage(message);
    }

    public void sendMessage(String message) {
        HttpEntity<String> entity = new HttpEntity<>(message, getHeaders());
        restTemplate.exchange(broadcastUrl, HttpMethod.POST, entity, String.class);
    }

    private HttpHeaders getHeaders() {
        Vault vaultProperties = vaultService.getVaultProperties();
        HttpHeaders headers = new HttpHeaders();
        headers.set(tokenHeaderName, vaultProperties.getToken());
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }
}
