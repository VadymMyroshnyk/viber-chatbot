package com.spdu.service.communication;

import com.spdu.config.Vault;
import com.spdu.service.VaultService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ViberMessageDeliveryService implements MessageDeliveryService {

    @Value("${viber.bot.header}")
    private String tokenHeaderName;
    @Value("${viber.bot.message.url}")
    private String url;

    private final VaultService vaultService;
    private final RestTemplate restTemplate;

    public ViberMessageDeliveryService(VaultService vaultService, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.vaultService = vaultService;
    }

    @SneakyThrows
    public void send(String message) {
        HttpEntity<String> entity = new HttpEntity<>(message, getHeaders());
        restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }

    private HttpHeaders getHeaders() {
        Vault vaultProperties = vaultService.getVaultProperties();
        HttpHeaders headers = new HttpHeaders();
        headers.set(tokenHeaderName, vaultProperties.getToken());
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }
}
