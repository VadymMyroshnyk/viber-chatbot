package com.spdu.state.needhelp;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;
import com.spdu.entity.HelpRequest;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

public class NeedHelpState extends BotState {

    public NeedHelpState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        return switch (action) {
            case MAIN_MENU -> new MainMenuState(session);
            case USER_INPUT -> {
                saveCustomerName(request.getMessage());
                yield new RequestTitleState(session);
            }
            default -> this;
        };
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        final Message message = session.getContext()
            .messageFactory()
            .needHelp(sender);
        return objectMapper.writeValueAsString(message);
    }

    private void saveCustomerName(Message message) {
        HelpRequest helpRequest = new HelpRequest();
        helpRequest.setHelpRecipient(message.getText());

        session.setHelpRequest(helpRequest);
    }
}
