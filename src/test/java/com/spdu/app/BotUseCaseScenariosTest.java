package com.spdu.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;
import com.spdu.service.communication.message.MessageFactory;
import com.spdu.testconfig.AppTest;
import com.spdu.testdouble.MessageDeliveryServiceSpy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.spdu.testbuilder.RequestBuilder.aRequest;
import static org.assertj.core.api.Assertions.assertThat;

@AppTest
class BotUseCaseScenariosTest {

    private static int ID = 1;

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private MessageDeliveryServiceSpy spy;
    @Autowired
    private MessageFactory messageFactoryStub;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final String senderId = nextSender();
    private final Sender sender = Sender.builder()
        .id(senderId)
        .build();

    private String context;

    @BeforeEach
    void givenWebContext() {
        context = "http://localhost:" + port + "/";
    }

    @Test
    @DisplayName("User can start conversation with the bot")
    void startConversation() {
        final ResponseEntity<String> response = restTemplate.postForEntity(context, aRequest()
            .event("conversation_started")
            .build(), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).contains("Слава Україні!");
    }

    @Test
    @DisplayName("Given initial state When user click Почати Then show main menu")
    void beginWithMainMenu() throws JsonProcessingException {
        messageWebhook(request("begin"));

        final String actual = spy.getCalledWith();
        final String expected = convert(messageFactoryStub.mainMenu(sender));
        assertThat(actual).isEqualTo(expected);
    }

    @Nested
    @DisplayName("Given MainMenu state")
    class MainMenuContext {
        @BeforeEach
        void setup() {
            messageWebhook(request("begin"));
        }

        @Nested
        @DisplayName("Given Info state")
        class InfoStateContext {
            @BeforeEach
            void setup() {
                messageWebhook(request("INFO"));
            }

            @Test
            @DisplayName("When invalid input Then keep INFO state")
            void invalidInputInInfoState() throws JsonProcessingException {
                messageWebhook(request("random message"));

                final String actual = spy.getCalledWith();
                final String expected = chain(
                    messageFactoryStub.mainMenu(sender),
                    messageFactoryStub.info(sender),
                    messageFactoryStub.info(sender)
                );
                assertThat(actual).isEqualTo(expected);
            }
        }

        @Test
        @DisplayName("When INFO received Then send message with information about bot")
        void botInfo() throws JsonProcessingException {
            messageWebhook(request("INFO"));

            final String actual = spy.getCalledWith();
            final String expected = chain(
                messageFactoryStub.mainMenu(sender),
                messageFactoryStub.info(sender)
            );
            assertThat(actual).isEqualTo(expected);
        }

        @Test
        @DisplayName("When NEED_HELP received Then show prompt for user input")
        void needHelpPrompt() throws JsonProcessingException {
            messageWebhook(request("NEED_HELP"));

            final String actual = spy.getCalledWith();
            final String expected = chain(
                messageFactoryStub.mainMenu(sender),
                messageFactoryStub.needHelp(sender)
            );
            assertThat(actual).isEqualTo(expected);
        }
    }

    private String nextSender() {
        return "test-sender-" + ID++;
    }

    private void messageWebhook(RequestDto requestDto) {
        final ResponseEntity<String> response = restTemplate.postForEntity(context, requestDto, String.class);
        assertThat(response.getStatusCode())
            .as("Any webhook request should return OK status")
            .isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
            .as("Webhook with message should return null body on success execution")
            .isNull();
    }

    private String chain(Message... m) throws JsonProcessingException {
        String result = "";
        for (Message message : m) {
            if (!result.isEmpty()) {
                result += '\n';
            }
            result += convert(message);
        }
        return result;
    }

    private String convert(Message m) throws JsonProcessingException {
        return objectMapper.writeValueAsString(m);
    }

    private RequestDto request(String message) {
        return aRequest()
            .event("message")
            .sender(senderId)
            .message(message)
            .build();
    }
}
