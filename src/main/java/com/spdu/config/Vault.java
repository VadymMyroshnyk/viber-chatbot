package com.spdu.config;

import lombok.Data;

@Data
public class Vault {

    private String token;
}
