package com.spdu.state;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;

public abstract class BotState {

    protected final Session session;
    protected final ObjectMapper objectMapper;

    protected BotState(Session session) {
        this.session = session;
        this.objectMapper = new ObjectMapper();
    }

    public abstract BotState handle(Action action, RequestDto request);

    public abstract String getMessage(Sender sender);
}
