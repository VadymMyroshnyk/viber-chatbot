package com.spdu.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spdu.broadcast.UsersRegistry;
import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.repository.HelpRequestRepository;
import com.spdu.service.communication.MessageDeliveryService;
import com.spdu.service.communication.message.MessageFactory;
import com.spdu.state.Action;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class BotService {

    private static final String EMPTY = "";

    @Value("${minio.bucket.path}")
    private String minioBucketPath;

    private final MessageDeliveryService messageDelivery;
    private final Map<Sender, Session> sessionBySender;
    private final ObjectMapper objectMapper;
    private final ImageStorageService imageStorageService;
    private final HelpRequestRepository helpRequestRepository;
    private final MessageFactory messageFactory;
    private final UsersRegistry usersRegistry;

    public BotService(
        MessageDeliveryService messageDelivery,
        ImageStorageService imageStorageService,
        HelpRequestRepository helpRequestRepository,
        MessageFactory messageFactory,
        UsersRegistry usersRegistry) {
        this.messageDelivery = messageDelivery;
        this.imageStorageService = imageStorageService;
        this.helpRequestRepository = helpRequestRepository;
        this.messageFactory = messageFactory;
        this.usersRegistry = usersRegistry;
        this.sessionBySender = new ConcurrentHashMap<>();
        this.objectMapper = new ObjectMapper();
    }

    public String incoming(RequestDto request) {
        CallbackType callbackType = CallbackType.of(request.getEvent());
        Sender sender = request.getSender();

        return switch (callbackType) {
            case MESSAGE -> {
                usersRegistry.register(sender);
                final Session session = sessionBySender.computeIfAbsent(sender, this::newSession);
                final String message = session.handle(request);
                messageDelivery.send(message);
                yield EMPTY;
            }
            case CONVERSATION_STARTED -> startMessage();
            default -> EMPTY;
        };
    }

    private Session newSession(Sender sender) {
        final Session session = new Session(sender);
        session.setState(new MainMenuState(session));
        final Context context = new Context(
            imageStorageService,
            helpRequestRepository,
            minioBucketPath,
            messageFactory
        );
        session.setContext(context);
        return session;
    }

    @SneakyThrows
    private String startMessage() {
        Button button = Button.builder()
            .frame(Frame.builder().build())
            .text("<font color=\"#1133dd\">Почати</font>")
            .actionBody(Action.MAIN_MENU.name())
            .build();

        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(button))
            .build();

        Message message = Message.builder()
            .text(getText())
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(message);
    }

    private String getText() {
        return """
            Слава Україні! 🇺🇦
                        
            Вас вітає чат-бот "Допомагаємо Разом" 👋
            🔗Ми звʼязуємо тих, хто хоче допомогти з тими, кому потрібна допомога.
            🤖️️Цей бот обʼєднує волонтерів, які збирають кошти на потреби військових з людьми, які хочуть
            задонатити на добру справу.
                        
            Разом ми сила!💪""";
    }
}
