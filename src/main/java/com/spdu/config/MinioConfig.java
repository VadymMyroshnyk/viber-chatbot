package com.spdu.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfig {

    @Value("${minio.endpoint}")
    private String endpoint;
    @Value("${minio.port}")
    private Integer port;
    @Value("${minio.accessKey}")
    private String accessKey;
    @Value("${minio.secretKey}")
    private String secretKey;
    @Value("${minio.secure}")
    private boolean secure;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
            .credentials(accessKey, secretKey)
            .endpoint(endpoint, port, secure)
            .build();
    }
}
