package com.spdu.state.needhelp;

import com.spdu.dto.RequestDto;
import com.spdu.entity.HelpRequest;
import com.spdu.dto.message.Button;
import com.spdu.dto.message.Frame;
import com.spdu.dto.message.Message;
import com.spdu.dto.message.MessageKeyboard;
import com.spdu.dto.Sender;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.menu.MainMenuState;
import lombok.SneakyThrows;

import java.util.List;

import static com.spdu.state.TagBuilder.blueFont;

public class CardState extends BotState {

    protected CardState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        return switch (action) {
            case MAIN_MENU -> new MainMenuState(session);
            case USER_INPUT -> {
                saveCardNumber(request.getMessage());
                yield new ContactState(session);
            }
            default -> this;
        };
    }

    @SneakyThrows
    public String getMessage(Sender sender) {
        MessageKeyboard keyboard = MessageKeyboard.builder()
            .buttons(List.of(createButton(Action.MAIN_MENU.name())))
            .build();

        Message message = Message.builder()
            .receiver(sender.getId())
            .text("""
                ✅Реквізити, куди надсилати гроші?
                                
                💡Приклад:
                💵Номер картки: 1111 2222 3333 4444
                """)
            .keyboard(keyboard)
            .build();

        return objectMapper.writeValueAsString(message);
    }

    private Button createButton(String actionBody) {
        return Button.builder()
            .frame(Frame.builder().build())
            .text(blueFont("Меню"))
            .actionBody(actionBody)
            .columns(6)
            .build();
    }

    private void saveCardNumber(Message message) {
        HelpRequest helpRequest = session.getHelpRequest();
        helpRequest.setCardNumber(message.getText());
    }
}
