package com.spdu.state.menu;

import com.spdu.dto.RequestDto;
import com.spdu.dto.Sender;
import com.spdu.dto.message.Message;
import com.spdu.state.Action;
import com.spdu.state.BotState;
import com.spdu.state.Session;
import com.spdu.state.canhelp.CanHelpState;
import com.spdu.state.info.InfoState;
import com.spdu.state.needhelp.NeedHelpState;
import com.spdu.state.statistics.StatisticState;
import lombok.SneakyThrows;

public class MainMenuState extends BotState {

    public MainMenuState(Session session) {
        super(session);
    }

    @Override
    public BotState handle(Action action, RequestDto request) {
        return switch (action) {
            case INFO -> new InfoState(session);
            case NEED_HELP -> new NeedHelpState(session);
            case CAN_HELP -> new CanHelpState(session);
            case STATISTIC -> new StatisticState(session);
            default -> this;
        };
    }

    @Override
    @SneakyThrows
    public String getMessage(Sender sender) {
        final Message menu = session.getContext()
            .messageFactory()
            .mainMenu(sender);

        return objectMapper.writeValueAsString(menu);
    }
}
