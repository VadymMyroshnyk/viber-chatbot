package com.spdu.service.communication;

public interface MessageDeliveryService {

    void send(String message);
}
