package com.spdu.dto;

import com.spdu.dto.message.Message;
import lombok.Data;

@Data
public class RequestDto {

    private String event;
    private Sender sender;
    private Message message;
}
