package com.spdu.service;

import com.spdu.config.Vault;
import com.spdu.exception.VaultException;
import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultOperations;
import org.springframework.vault.core.VaultVersionedKeyValueTemplate;
import org.springframework.vault.support.Versioned;

import java.util.Objects;

@Service
public class VaultService {

    private static final String VAULT_PATH_PREFIX = "secret";

    private final VaultVersionedKeyValueTemplate vaultTemplate;

    public VaultService(VaultOperations vaultOperations) {
        this.vaultTemplate = new VaultVersionedKeyValueTemplate(vaultOperations, VAULT_PATH_PREFIX);
    }

    public Vault getVaultProperties() {
        Versioned<Vault> vaultVersioned = vaultTemplate.get("/viber", Vault.class);

        if (Objects.isNull(vaultVersioned)) {
            throw new VaultException("Can't get properties from Vault");
        }

        return vaultVersioned.getData();
    }
}
