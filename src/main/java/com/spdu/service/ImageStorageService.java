package com.spdu.service;

public interface ImageStorageService {

    String upload(String imgUrl);
}
