package com.spdu.controller;

import com.spdu.dto.RequestDto;
import com.spdu.service.BotService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViberController {

    private final BotService botService;

    public ViberController(BotService botService) {
        this.botService = botService;
    }

    @PostMapping("/")
    public String incoming(@RequestBody RequestDto request) {
        return botService.incoming(request);
    }
}
